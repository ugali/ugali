from django.shortcuts import render

# Create your views here.
def home(req):
	menu = 'Vicoba'
	return render(req,'index.html',{'menu':menu,'id':1})


def daushop(req):
	menu = 'DauShop'
	return render(req,'index.html',{'menu':menu,'id':2})


def taasisi(req):
	menu = 'Taasisi'
	return render(req,'index.html',{'menu':menu,'id':3})


def tuma(req):
	menu = 'Tuma Pesa'
	return render(req,'index.html',{'menu':menu,'id':4})


def toa(req):
	menu = 'Toa Pesa'
	return render(req,'index.html',{'menu':menu,'id':5})


def users(req):
	menu = 'Users'
	return render(req,'index.html',{'menu':menu,'id':6})


def setting(req):
	menu = 'Setting'
	return render(req,'index.html',{'menu':menu,'id':7})