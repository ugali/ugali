from __future__ import unicode_literals
from datetime import datetime
from django.db import models


# Create your models here.
class UssdLogs(models.Model):
    _datetime = models.DateTimeField(default=datetime.now)
    _code = models.CharField(max_length=100)
    _msisdn = models.CharField(max_length=20)
    _imsi = models.CharField(max_length=100)
    _sess = models.CharField(max_length=100)
    _reqs = models.IntegerField()

    class Meta:
        db_table = 'tb_logs'


class UssdDbMenu(models.Model):
    _code = models.CharField(primary_key=True, max_length=20)
    _menu = models.TextField()

    class Meta:
        db_table = 'tb_menu'
