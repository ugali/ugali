import os

from django.http import HttpResponse


class UssdComn:
    def __init__(self):
        os.environ['TZ'] = 'Africa/Nairobi'

    def out(self, msg):
        return HttpResponse(
            "<?xml version='1.0' encoding='utf-8'?><ussd><message>%s</message><flow>cont</flow></ussd>" % msg)

    def close(self, msg):
        return HttpResponse(
            "<?xml version='1.0' encoding='utf-8'?><ussd><message>%s</message><flow>close</flow></ussd>" % msg)
