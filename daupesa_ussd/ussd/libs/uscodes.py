from ussd.models import UssdLogs


class UssdCodes:
    def __init__(self, msisdn, imsi, _sess, reqs):
        self.ms = msisdn
        self.im = imsi
        self.se = _sess
        self.re = reqs

    def put(self, code):

        last_input = self.get()  # Get Last Code

        if last_input == "Not Found":
            code = code  # Code = *150*47
        elif code == '0' and last_input != '*150*47':
            code = last_input[0:len(last_input) - 2]  # Go Back
        else:
            code = last_input + "*" + code  # Create New Code
            if code == '*150*47*0':
                code = '*150*47'

        md = UssdLogs(_code=code, _msisdn=self.ms, _imsi=self.im, _sess=self.se, _reqs=self.re)
        md.save()

        return

    def get(self):
        try:
            md = UssdLogs.objects.filter(_msisdn=self.ms, _imsi=self.im, _sess=self.se).last()
            return md._code
        except Exception as e:
            return "Not Found"
