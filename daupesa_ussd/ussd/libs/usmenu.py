import os

from ussd.models import UssdDbMenu


class UssdMenu:
    def __init__(self):
        os.environ['TZ'] = 'Africa/Nairobi'
        self.createTemp()

    def create(self, code, menu):
        db = UssdDbMenu(_code=code, _menu=menu)
        db.save()
        return

    def display(self, code):
        try:
            db = UssdDbMenu.objects.filter(_code=code).last()
            return db._menu
        except Exception as e:
            return "Chaguo si Sahihi\n0. Rudi Nyuma"

    def createTemp(self):
        self.create("*150*47",
                    "Karibu Daupesa\n\n1. Vicoba\n2. Daushop\n3. Taasisi\n4. Tuma Pesa\n5. Toa Pesa\n6. Akaunti Yangu")
        self.create("*150*47*0",
                    "Karibu Daupesa\n\n1. Vicoba\n2. Daushop\n3. Taasisi\n4. Tuma Pesa\n5. Toa Pesa\n6. Akaunti Yangu")
        self.create("*150*47*1",
                    "1. Weka Akiba\n2. Omba Mkopo\n3. Marejesho\n4. Omba Mashine\n5. Weka Akiba\n0. Rudi Nyuma")
        self.create("*150*47*1*1", "Andika Namba ya Kikundi\n0. Rudi Nyuma")
        self.create("*150*47*1*2", "Andika Namba ya Kikundi\n0. Rudi Nyuma")
        self.create("*150*47*1*3", "Andika Namba ya Kikundi\n0. Rudi Nyuma")

        self.create("*150*47*2", "1. Lipa\n2. Salio")
        self.create("*150*47*3", "1. Efrazia")
        self.create("*150*47*4", "Namba ya Mpokeaji")
        self.create("*150*47*5", "Namba ya Wakala")
        self.create("*150*47*6", "1.Salio")
        return
