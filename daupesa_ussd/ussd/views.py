from django.shortcuts import render
from libs.common import UssdComn
from libs.uscodes import UssdCodes
from libs.usmenu import UssdMenu


# Create your views here.
def home(req):
    menutitle = "Vicoba"
    return render(req, 'index.html', {'menutitl': menutitle})


def vicoba(req):
    menutitle = "Vicoba "
    return render(req, 'index.html', {'menutitl': menutitle})


def daushop(req):
    menutitl = "Daushop "
    return render(req, 'index.html', {'menutitl': menutitl})


def ussd(req, code, msisdn, imsi, _sess, reqs):
    us = UssdComn()  # Common FX 	( out(),close() ..... )
    cd = UssdCodes(msisdn, imsi, _sess, reqs)  # Create Codes ( Start,Next,Back .... )
    mn = UssdMenu()  # Display Menu ( Vicoba,Daushop,Taasisi ..... )

    cd.put(code)  # Cache Input

    return us.out(mn.display(cd.get()))  # Display Menu by Cached CODE
